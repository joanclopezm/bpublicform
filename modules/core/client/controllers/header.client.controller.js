(function () {
  'use strict';

  angular
    .module('core')
    .controller('HeaderController', HeaderController);

  HeaderController.$inject = ['$scope', '$state', 'Authentication', 'menuService','$location','$rootScope'];

  function HeaderController($scope, $state, Authentication, menuService,$location,$rootScope) {
    var vm = this;
    console.log($location.url())
    console.log($location.path())
    var path = $location.url()

    var filterPath = function (path){
      var split = path.split('/')
      return (split.indexOf('customers')>-1&&(split.indexOf('edit')>-1||split.indexOf('create')>-1||split.indexOf('pdf')>-1))||split.indexOf('signin')>-1
    }

    $rootScope.$on("$locationChangeStart", function(event, next, current) {
       // handle route changes
       //console.log($location.url())
       path = $location.url()
       vm.displaySideBar=filterPath(path)

   });

    vm.displaySideBar=filterPath(path)
    vm.accountMenu = menuService.getMenu('account').items[0];
    vm.authentication = Authentication;
    vm.isCollapsed = false;
    vm.menu = menuService.getMenu('topbar');

    $scope.$on('$stateChangeSuccess', stateChangeSuccess);

    function stateChangeSuccess() {
      // Collapsing the menu after navigation
      vm.isCollapsed = false;
    }
  }
}());
