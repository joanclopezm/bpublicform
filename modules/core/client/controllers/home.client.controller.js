(function () {
  'use strict';

  angular
    .module('core')
    .controller('HomeController', HomeController);
    HomeController.$inject = ['$scope','Authentication', 'CustomersService','AdminService'];

  function HomeController($scope, Authentication, CustomersService, AdminService) {
    var vm = this;
    vm.authentication = Authentication;
    vm.customers = CustomersService.query();
    console.log(vm.customers);
    vm.users=AdminService.query();
    vm.filterApproved = function(){
        return vm.customers.filter(function(value){
          return value.approved && value.approvedAt ? moment(value.approvedAt).isAfter(moment().startOf('month')):false
        });
    };

    vm.filterIncomplete = function(){
      return vm.customers.filter(function(value){
        return !value.approved
      })
    }

    vm.filterByRole = function(role){
      return vm.users.filter(function(user){
         return user.roles.indexOf(role)>-1
       })
    }


  }
}());
