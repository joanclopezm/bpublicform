'use strict';

var validator = require('validator'),
  path = require('path'),
  config = require(path.resolve('./config/config')),
  nodemailer = require('nodemailer'),
  doctors = require('../../../doctors/server/controllers/doctors.server.controller'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  async = require('async');


  /**
 * proces email pettiotions of shopify
 */
  var smtpTransport = nodemailer.createTransport(config.mailer.options);
  exports.teleconsultation= function (req, res){
    var emails = [];
    async.waterfall([
     //get doctors from db in ourwebsite
      function (done) {
        var shopifyIds = [];
        req.body.line_items.forEach(element => {
          shopifyIds.push({'shopifyId' : element.product_id});
        })
        doctors.getSpecificDoctors(shopifyIds, function(err,doctors){
          done(err,shopifyIds, doctors);
        });
      },
      function (shopifyIds, doctors, done) {
        console.log(doctors)
        async.each(doctors, function(doctor,callback){
          res.render(path.resolve('modules/core/server/templates/videoconsulta'), {
            name : doctor.name,
            link : doctor.link,
            shopifyId : doctor.shopifyId
          }, function (err, emailHTML) {
              emails.push({'name':doctor.name,'emailRender':emailHTML})
              callback(err);
          });
        },function (err) {
          if (err) {
            console.log(err)
          }else{
            done(err, shopifyIds,doctors)
          }
        })
      },
      function (shopifyIds,doctors,done) {
        console.log('emails')
        console.log(emails)
        async.each(emails, function(email,callback){
          var mailOptions = {
            to: req.body.email,
            from: config.mailer.from,
            subject: 'Agenda tu cita con '+ email.name,
            html: email.emailRender
          };
          console.log(mailOptions)
          smtpTransport.sendMail(mailOptions, function (err) {
            if (!err) {
              callback()
            } else {
              callback(err)
            }
          });
        },function (err) {
          if (err) {
            console.log(err)
          }else{
            done(err,doctors)
          }
        })
        
      }
    ], function (err) {
      if (err) {
        console.log(err)
      }else{
        res.status(200).send('OK')

      }
    });
    

  }

  exports.paidEmail= function (req, res){
    console.log(req.body)
    var emailData = null;
    var image =null
    req.body.line_items.forEach(element => {
      console.log(element)
      switch (element.price) {
        case '250.00':
          image = ' https://cdn.shopify.com/s/files/1/0269/4512/6461/files/1.png?v=1586964417';
          break;

        case '500.00':
        image = 'https://cdn.shopify.com/s/files/1/0269/4512/6461/files/2.png?v=1586964418';
        break;

        case '750.00':
        image = 'https://cdn.shopify.com/s/files/1/0269/4512/6461/files/3.png?v=1586964422';
        break;

        case '1,900.00':
        image = 'https://cdn.shopify.com/s/files/1/0269/4512/6461/files/4.png?v=1586964420';
        break;

        case '4,750.00':
        image = 'https://cdn.shopify.com/s/files/1/0269/4512/6461/files/6.png?v=1586964417';
        break;

        case '9,500.00':
        image = 'https://cdn.shopify.com/s/files/1/0269/4512/6461/files/7.png?v=1586964420';
        break;

        case '14,250.00':
        image = 'https://cdn.shopify.com/s/files/1/0269/4512/6461/files/8.png?v=1586964422';
        break;

        case '19,000.00':
        image = 'https://cdn.shopify.com/s/files/1/0269/4512/6461/files/9.png?v=1586964419';
        break;

        case '28,500.00':
        image = 'https://cdn.shopify.com/s/files/1/0269/4512/6461/files/10.png?v=1586964422';
        break;

        case '47,500.00':
        image = 'https://cdn.shopify.com/s/files/1/0269/4512/6461/files/11.png?v=1586964422';
        break;

        default:
          console.log('indefault option switch')
          image = 'https://cdn.shopify.com/s/files/1/0269/4512/6461/files/comodin.png?v=1586969267';
          break;
      }
      console.log('Antes del if')
      console.log(element.title)
      if(element.title === 'Donativo COVID-19'){
      async.waterfall([
        function (done) {
          console.log('entro al waterfall')
          res.render(path.resolve('modules/core/server/templates/donativo'), {
            image : image
          }, function (err, emailHTML) {
            done(err, emailHTML);
          });
        },
        // If valid email, send reset email using service
        function (emailHTML, done) {
          var mailOptions = {
            to: req.body.email,
            from: config.mailer.from,
            subject: 'Gracias por tu donativo',
            html: emailHTML
          };
          smtpTransport.sendMail(mailOptions, function (err) {
            if (!err) {
              res.send({
                message: 'An email has been sent to the provided email with further instructions.'
              });
            } else {
              return res.status(400).send({
                message: 'Failure sending email'
              });
            }
    
            done(err);
          });
        }
      ], function (err) {
        if (err) {
          console.log(err)
        }
      });
      
    }
    
  });
  
  res.status(200).send('OK')
  }
        
/**
 * Render the main application page
 */
exports.renderIndex = function (req, res) {
  var safeUserObject = null;
  if (req.user) {
    safeUserObject = {
      displayName: validator.escape(req.user.displayName),
      provider: validator.escape(req.user.provider),
      username: validator.escape(req.user.username),
      created: req.user.created.toString(),
      roles: req.user.roles,
      profileImageURL: req.user.profileImageURL,
      email: validator.escape(req.user.email),
      lastName: validator.escape(req.user.lastName),
      firstName: validator.escape(req.user.firstName),
      additionalProvidersData: req.user.additionalProvidersData
    };
  }

  res.render('modules/core/server/views/index', {
    user: JSON.stringify(safeUserObject),
    sharedConfig: JSON.stringify(config.shared)
  });
};

/**
 * Render the server error page
 */
exports.renderServerError = function (req, res) {
  res.status(500).render('modules/core/server/views/500', {
    error: 'Oops! Something went wrong...'
  });
};

/**
 * Render the server not found responses
 * Performs content-negotiation on the Accept HTTP header
 */
exports.renderNotFound = function (req, res) {

  res.status(404).format({
    'text/html': function () {
      res.render('modules/core/server/views/404', {
        url: req.originalUrl
      });
    },
    'application/json': function () {
      res.json({
        error: 'Path not found'
      });
    },
    'default': function () {
      res.send('Path not found');
    }
  });
};
