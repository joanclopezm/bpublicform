﻿(function () {
  'use strict';

  // Configuring the Customers Admin module
  angular
    .module('customers.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Administrar Doctores',
      state: 'admin.customers.list'
    });
  }
}());
