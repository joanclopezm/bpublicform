(function () {
  'use strict';

  angular
    .module('customers.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('customers', {
        abstract: true,
        url: '/customers',
        template: '<ui-view/>'
      })
      .state('customers.list', {
        url: '',
        templateUrl: '/modules/customers/client/views/admin/list-customers.client.view.html',
        controller: 'CustomersAdminListController',
        data: {
          roles: ['admin']
        },
        controllerAs: 'vm'
      })
      // .state('customers.view', {
      //   url: '/:customerId',
      //   templateUrl: '/modules/customers/client/views/view-customer.client.view.html',
      //   controller: 'CustomersController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     customerResolve: getCustomer
      //   },
      //   data: {
      //     pageTitle: '{{ customerResolve.title }}'
      //   }
      // })
      .state('customers.create', {
        url: '/create',
        templateUrl: '/modules/customers/client/views/admin/form-customer.client.view.html',
        controller: 'CustomersAdminController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Creando Formulario'

        },
        resolve: {
          customerResolve: newCustomer
        }
      })
      .state('customers.edit', {
        url: '/:customerId/edit',
        templateUrl: '/modules/customers/client/views/admin/form-customer.client.view.html',
        controller: 'CustomersAdminController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Formulario Doctores'
        },
        resolve: {
          customerResolve: getCustomer
        }
      })
      .state('customers.pdf', {
        url: '/:customerId/pdf',
        templateUrl: '/modules/customers/client/views/admin/pdf-customer.client.view.html',
        controller: 'CustomersAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin'],
          pageTitle: 'Expor to Pdf'
        },
        resolve: {
          customerResolve: getCustomer
        }
      });
  }

  getCustomer.$inject = ['$stateParams', 'CustomersService'];

  function getCustomer($stateParams, CustomersService) {
    return CustomersService.get({
      customerId: $stateParams.customerId
    }).$promise;
  }
  newCustomer.$inject = ['CustomersService'];

  function newCustomer(CustomersService) {
    return new CustomersService();
  }
}());
