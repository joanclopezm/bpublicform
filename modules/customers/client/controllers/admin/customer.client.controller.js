(function () {
  'use strict';

  angular
    .module('customers.admin')
    .controller('CustomersAdminController', CustomersAdminController);

  CustomersAdminController.$inject = ['$scope', '$state', '$window', 'customerResolve', 'Authentication', 'Notification','Upload','$timeout'];

  function CustomersAdminController($scope, $state, $window, customer, Authentication, Notification,Upload,$timeout) {
    var vm = this;

    vm.customer = customer;
    vm.formView = 1;
    console.log(vm.customer)
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo']
    vm.ampms = ['AM','PM']
    vm.fonts= ['Montserrat','Times']
    vm.titles= ['Dr.','Dra.']
    vm.customer.title= vm.customer.title ? vm.customer.title : 'Dr.';
    vm.customer.font= vm.customer.font ? vm.customer.font :'Times';
    vm.promotions=['Gratis', '20%']
    vm.customer.promotion= vm.customer.promotion ? vm.customer.promotion : '20%'
    vm.customer.timeTable= vm.customer.timeTable ? vm.customer.timeTable :
    [{
      day:'Lunes',
      start: new Date(0, 0, 1, 2, 0),
      end: new Date(0, 0, 1, 2, 0),
    }]
    vm.customer.educations = vm.customer.educations ? vm.customer.educations : [];
    vm.customer.achievements = vm.customer.achievements ? vm.customer.achievements : [];
    vm.customer.otherServices = vm.customer.otherServices ? vm.customer.otherServices : [];
    vm.customer.listOfProcess = vm.customer.listOfProcess ? vm.customer.listOfProcess : [];
    vm.customer.commonMedicalConditions = vm.customer.commonMedicalConditions ? vm.customer.commonMedicalConditions : [];
    vm.customer.colorBackground = vm.customer.colorBackground ? vm.customer.colorBackground : 'azul';
    vm.service1= vm.customer.service&&vm.customer.service[0] ? vm.customer.service[0] : ''
    vm.service2= vm.customer.service&&vm.customer.service[1] ? vm.customer.service[1] : ''
    vm.service3= vm.customer.service&&vm.customer.service[2] ? vm.customer.service[2] : ''
    vm.customer.image1 = vm.customer.image1 ? vm.customer.image1 : '/modules/core/client/img/lp/dr_transparent.png'
    
    vm.autoCreateNewForm = function(){
      vm.save();
    }


    vm.addItemToList=function(item, list){
      console.log(list)
      list.push(item);
    }

    vm.gotoAnchor = function(x) {
      if ($location.hash() !== x) {
        // set the $location.hash to `newHash` and
        // $anchorScroll will automatically scroll to it
        $location.hash(x);
      } else {
        // call $anchorScroll() explicitly,
        // since $location.hash hasn't changed
        $anchorScroll();
      }
    };

    vm.remove = function(array,index) {
    //  var index = array.indexOf(item);
      array.splice(index, 1);
    }

    vm.countOf = function(text) {
      var s = text ? text.split(/\s+/) : 0; // it splits the text on space/tab/enter
      return s ? s.length : '';
    }
    vm.addSchedule= function (){
      vm.customer.timeTable.push({
        day:'Lunes',
        start:new Date(0, 0, 1, 2, 0),
        end:new Date(0, 0, 1, 2, 0)
      });
    }
    // Remove existing Customer
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.customer.$remove(function () {
          $state.go('admin.customers.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Customer deleted successfully!' });
        });
      }
    }

//35
    vm.formCalculationAdvance= function(){
      var completed = 0
      for(var key in vm.customer) {
        var obj = vm.customer[key];
        if(! (vm.customer[key] instanceof Function)){
           if(vm.customer[key]!=''&&vm.customer[key]!=null&& vm.customer[key].length>-1)
            completed++
        }
      }
      //35 numero total de casillas
      // vm.customer.percentage= (completed/35)*100

      return parseInt((completed/35)*100)
    }
    vm.formCalculationAdvance();
    vm.save=function(){
      vm.customer.service=[
          vm.service1,
          vm.service2

      ]
      console.log(vm.customer.timeTable)
      save();
    }

    // Save Customer
    function save() {
      // Create a new customer, or update the current instance
      vm.customer.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        //$state.go('admin.customers.list'); // should we send the User to the list or the updated Customer's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Guardado con exito' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Error al guardar' });
      }
    }

    vm.progress1 = 0;
    vm.progress2 = 0;
    vm.progress3 = 0;
    vm.progress4 = 0;
    vm.progress5 = 0;

    vm.printPDF = function(){
      window.print();
    }

    vm.upload = function (dataUrl , imageModel) {
      console.log(dataUrl)
      Upload.upload({
        url: '/api/customers/picture',
        data: {
          newProfilePicture: dataUrl
        }
      }).then(function (response) {
        $timeout(function () {
          switch(imageModel) {
            case 'image1':

              vm.customer.image1 = response.data;
            break;
            case 'image2':
            // code block
            vm.customer.image2 = response.data;
            console.log('vm customer 2',vm.customer.image2)
            break;
            case 'image3':
            // code block
            vm.customer.image3 = response.data;

            break;
            case 'image4':
            // code block
            vm.customer.image4 = response.data;

            break;
            case 'image5':
            vm.customer.image5 = response.data;
            break;
          }
          onSuccessItem(response.data, imageModel);
        });
      }, function (response) {
        if (response.status > 0) onErrorItem(response.data);
      }, function (evt) {
        console.log(evt)
        if (imageModel==='image1') {
          vm.progress = parseInt(100.0 * evt.loaded / evt.total, 10);
        }
        if (imageModel==='image2') {
          vm.progress2 = parseInt(100.0 * evt.loaded / evt.total, 10);
        }
        if (imageModel==='image3') {
          vm.progress3 = parseInt(100.0 * evt.loaded / evt.total, 10);
        }
        if (imageModel==='image4') {
          vm.progress4 = parseInt(100.0 * evt.loaded / evt.total, 10);
        }
        if (imageModel==='image5') {
          vm.progress5 = parseInt(100.0 * evt.loaded / evt.total, 10);
        }
      });
    };

    // Called after the user has successfully uploaded a new picture
    function onSuccessItem(response, imageModel) {
      // Show success message
      console.log(imageModel)
      vm.save();
      Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Successfully changed profile picture' });

      switch(imageModel) {
        case 'image1':
        vm.fileSelected = false;
        vm.progress = 0;
        break;
        case 'image2':
        // code block
        vm.fileSelected2 = false;
        vm.progress2 = 0;

        break;
        case 'image3':
        // code block
        vm.fileSelected3 = false;
        vm.progress3 = 0;

        break;
        case 'image4':
        // code block
        vm.fileSelected4 = false;
        vm.progress4 = 0;

        break;
        case 'image5':
        // code block
        vm.fileSelected5 = false;
        vm.progress5 = 0;

        break;
        // code block
      }
      // Reset form

    }

    // Called after the user has failed to upload a new picture
    function onErrorItem(response, imageModel) {
      switch(imageModel) {
        case 'image1':
        vm.fileSelected = false;
        vm.progress = 0;
        break;
        case 'image2':
        // code block
        vm.fileSelected2 = false;
        vm.progress2 = 0;

        break;
        case 'image3':
        // code block
        vm.fileSelected3 = false;
        vm.progress3 = 0;

        break;
        case 'image4':
        // code block
        vm.fileSelected4 = false;
        vm.progress4 = 0;

        break;
        case 'image5':
        // code block
        vm.fileSelected5 = false;
        vm.progress5 = 0;

        break;
        // code block
      }

      // Show error message
      Notification.error({ message: response.message, title: '<i class="glyphicon glyphicon-remove"></i> Failed to change profile picture' });
    }
  }
}());
