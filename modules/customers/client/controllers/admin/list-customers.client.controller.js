﻿(function () {
  'use strict';

  angular
    .module('customers.admin')
    .controller('CustomersAdminListController', CustomersAdminListController);

  CustomersAdminListController.$inject = ['CustomersService', '$filter'];

  function CustomersAdminListController(CustomersService, $filter) {
    var vm = this;



    vm.buildPager = buildPager;
    vm.figureOutItemsToDisplay = figureOutItemsToDisplay;
    vm.pageChanged = pageChanged;

    CustomersService.query(function (data) {
      vm.customers = data;
      vm.buildPager();
    });

    function buildPager() {
      vm.pagedItems = [];
      vm.itemsPerPage = 15;
      vm.currentPage = 1;
      vm.figureOutItemsToDisplay();
    }

    function figureOutItemsToDisplay() {
      vm.filteredItems = $filter('filter')(vm.customers, {
        $: vm.search
      });
      vm.filterLength = vm.filteredItems.length;
      var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
      var end = begin + vm.itemsPerPage;
      vm.pagedItems = vm.filteredItems.slice(begin, end);
    }

    function pageChanged() {
      vm.figureOutItemsToDisplay();
    }

    vm.formCalculationAdvance= function(customer){
      var completed = 0
      for(var key in customer) {
        var obj = customer[key];
        if(! (customer[key] instanceof Function)){
           if(customer[key]!=''&&customer[key]!=null&& customer[key].length>-1)
            completed++
        }
      }
      console.log((completed/35)*100)
      //35 numero total de casillas
      // vm.customer.percentage= (completed/35)*100

      return parseInt((completed/35)*100)
    }
  }
}());
