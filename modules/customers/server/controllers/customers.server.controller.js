'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  config = require(path.resolve('./config/config')),
  multer = require('multer'),
  Customer = mongoose.model('Customer'),
  cloudinary = require('cloudinary').v2,
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));


/**
 * Create an customer
 */
exports.create = function (req, res) {
  var customer = new Customer(req.body);
  customer.user = req.user;

  customer.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(customer);
    }
  });
};

/**
 * Show the current customer
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var customer = req.customer ? req.customer.toJSON() : {};

  // Add a custom field to the Customer, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Customer model.
  customer.isCurrentUserOwner = !!(req.user && customer.user && customer.user._id.toString() === req.user._id.toString());

  res.json(customer);
};

/**
 * Update an customer
 */
exports.update = function (req, res) {
  var customer = req.customer;

  customer.approved = req.body.approved;
  customer.colorBackground = req.body.colorBackground;
  customer.created = req.body.created;
  customer.educations = req.body.educations;
  customer.lastModification = req.body.lastModification;
  customer.lastName = req.body.lastName;
  customer.name = req.body.name;
  customer.officeAddress = req.body.officeAddress;
  customer.officeData = req.body.officeData;
  customer.officeName = req.body.officeName;
  customer.otherOfficeAddress = req.body.otherOfficeAddress;
  customer.otherOfficeData = req.body.otherOfficeData;
  customer.otherOfficeName = req.body.otherOfficeName;
  customer.otherServices = req.body.otherServices;
  customer.professionalId = req.body.professionalId;
  customer.professionalInsight = req.body.professionalInsight;
  customer.service = req.body.service;
  customer.specialty = req.body.specialty;
  customer.specialtyDescription = req.body.specialtyDescription;
  customer.specialtyProfessionalId = req.body.specialtyProfessionalId;
  customer.timeTable = req.body.timeTable;
  customer.university = req.body.university;
  customer.promotion = req.body.promotion;
  customer.font = req.body.font;
  customer.email = req.body.email
  customer.phone = req.body.phone
  customer.mobile = req.body.mobile
  customer.facebook = req.body.facebook
  customer.officePhone = req.body.officePhone
  customer.extension = req.body.extension
  customer.image1 = req.body.image1
  customer.image2 = req.body.image2
  customer.image3 = req.body.image3
  customer.image4 = req.body.image4
  customer.image5 = req.body.image5
  customer.workInOtherOffice = req.body.workInOtherOffice
  customer.costsOfConsultation = req.body.costsOfConsultation
  customer.descriptionOfConsultation = req.body.descriptionOfConsultation
  customer.aboutYou = req.body.aboutYou
  customer.achievements = req.body.achievements
  customer.listOfProcess = req.body.listOfProcess
  customer.commonMedicalConditions = req.body.commonMedicalConditions
  customer.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(customer);
    }
  });
};

/**
 * Delete an customer
 */
exports.delete = function (req, res) {
  var customer = req.customer;

  customer.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(customer);
    }
  });
};

/**
 * List of Customers
 */
exports.list = function (req, res) {
  Customer.find().sort('-created').populate('user', 'displayName').exec(function (err, customers) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(customers);
    }
  });
};

/**
 * Customer middleware
 */
exports.customerByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Customer is invalid'
    });
  }

  Customer.findById(id).populate('user', 'displayName').exec(function (err, customer) {
    if (err) {
      return next(err);
    } else if (!customer) {
      return res.status(404).send({
        message: 'No customer with that identifier has been found'
      });
    }
    req.customer = customer;
    next();
  });
};





/**
 * Update profile picture
 */
exports.picture = function (req, res) {
  // set your env variable CLOUDINARY_URL or set the following configuration
  cloudinary.config({
    cloud_name: 'dibkpzdtu',
    api_key: '951286982366453',
    api_secret: 'NyhMVzvBuqPseNQuKXfDT7LkCP4'

  });
  var urlImage = '';
  var existingImageUrl;
  var multerConfig;



    multerConfig = config.uploads.profile.image;


  // Filtering to upload only images
  multerConfig.fileFilter = require(path.resolve('./config/lib/multer')).imageFileFilter;

  var upload = multer(multerConfig).single('newProfilePicture');

    //existingImageUrl = user.profileImageURL;
    uploadImage()
      .then(setCloudinary)
      .then(function () {
        console.log('thisiiiii', urlImage)
        res.json(urlImage);
      })
      .catch(function (err) {
        console.log(err)
        res.status(422).send(err);
      });


  function uploadImage() {
    return new Promise(function (resolve, reject) {
      upload(req, res, function (uploadError) {
        if (uploadError) {
          reject(errorHandler.getErrorMessage(uploadError));
        } else {
          resolve();
        }
      });
    });
  }

  function setCloudinary() {
    return new Promise(function (resolve, reject) {
      console.log(req.file)
      cloudinary.uploader.upload(req.file.path, function (err, image) {
        console.log("** File Upload");
        console.log("* public_id for the uploaded image is generated by Cloudinary's service.");
        console.log("* " + image.public_id);
        console.log("* " + image.url);
        urlImage= image.url;
        if (err) { console.log(err);
          reject(err)
        }else{
          console.log('resolve')
          resolve();
        }
      });



    });
  }

}
