'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  path = require('path'),
  config = require(path.resolve('./config/config')),
  chalk = require('chalk');

/**
 * Customer Schema
 */
var CustomerSchema = new Schema({
  progress: {
      type: Number,
      trim: true
  },
  name: {
      type: String,
      trim: true
  },
  lastName: {
      type: String,
      trim: true
  },
  university: {
      type: String,
      trim: true
  },
  //cedula profesional
  professionalId: {
      type: String,
      trim: true
  },
  officeName: {
      type: String,
      trim: true
  },
  officeAddress: {
      type: String,
      trim: true
  },
  //datoa de la oficina, "piso numero etc"
  officeData: {
      type: String,
      trim: true
  },
  otherOfficeName: {
      type: String,
      trim: true
  },
  otherOfficeAddress: {
      type: String,
      trim: true
  },
  //datoa de la oficina, "piso numero etc"
  otherOfficeData: {
      type: String,
      trim: true
  },
  //cedula profesional de la especialidad
  specialtyProfessionalId: {
      type: String,
      trim: true
  },
  specialtyDescription: {
      type: String,
      trim: true
  },
  timeTable:{
    type:Array,
    trim:true
  },
  email: {
      type: String,
      trim: true
  },
  phone: {
      type: String,
      trim: true
  },
  //celular
  mobile: {
      type: String,
      trim: true
  },
  facebook: {
      type: String,
      trim: true
  },
  officePhone: {
      type: String,
      trim: true
  },
  workInOtherOffice: {
      type: Boolean,
      trim: true
  },
  costsOfConsultation:{
      type: Number,
      trim: true
  },
  descriptionOfConsultation:{
      type: String,
      trim: true
  },
  extension: {
      type: String,
      trim: true
  },
  aboutYou: {
      type: String,
      trim: true
  },
  address: {
      type: String,
      trim: true
  },
  arrangement: {
      type: String,
      trim: true
  },
  huliId: {
      type: String,
      trim: true
  },
  paymentDay: {
      type: String,
      trim: true
  },
  seller: {
      type: String,
      trim: true
  },
  font: {
      type: String,
      trim: true
  },
  state: {
      type: String,
      trim: true
  },
  delegation: {
      type: String,
      trim: true
  },
  huliPhone: {
      type: String,
      trim: true
  },
  promotion: {
      type: String,
      trim: true
  },
  image1: {
      type: String,
      trim: true
  },
  image2: {
      type: String,
      trim: true
  },
  image3: {
      type: String,
      trim: true
  },
  image4: {
      type: String,
      trim: true
  },
  image5: {
      type: String,
      trim: true
  },
  specialty: {
      type: String,
      trim: true,
      "default": ""
  },
  service: {
      type: Array
  },
  otherServices: {
      type: Array,
      trim: true

  },
  educations: {
      type: Array

  },
  achievements:  {
      type: Array

  },
  listOfProcess:  {
      type: Array

  },
  commonMedicalConditions:  {
      type: Array
  },
  professionalInsight: {
       type: String,
       trim: true,
       "default": ""
  },
  colorBackground: {
       type: String,
       trim: true,
       "default": "Azul"
  },
  approved: {
      type: Boolean,
      "default": false
  },
  approvedAt: {
      type: Date
  },
  lastModification: {
      type: Date,
      "default": Date.now
  },
  created: {
      type: Date,
      "default": Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

CustomerSchema.statics.seed = seed;

mongoose.model('Customer', CustomerSchema);

/**
* Seeds the User collection with document (Customer)
* and provided options.
*/
function seed(doc, options) {
  var Customer = mongoose.model('Customer');

  return new Promise(function (resolve, reject) {

    skipDocument()
      .then(findAdminUser)
      .then(add)
      .then(function (response) {
        return resolve(response);
      })
      .catch(function (err) {
        return reject(err);
      });

    function findAdminUser(skip) {
      var User = mongoose.model('User');

      return new Promise(function (resolve, reject) {
        if (skip) {
          return resolve(true);
        }

        User
          .findOne({
            roles: { $in: ['admin'] }
          })
          .exec(function (err, admin) {
            if (err) {
              return reject(err);
            }

            doc.user = admin;

            return resolve();
          });
      });
    }

    function skipDocument() {
      return new Promise(function (resolve, reject) {
        Customer
          .findOne({
            title: doc.title
          })
          .exec(function (err, existing) {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove Customer (overwrite)

            existing.remove(function (err) {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise(function (resolve, reject) {
        if (skip) {
          return resolve({
            message: chalk.yellow('Database Seeding: Customer\t' + doc.title + ' skipped')
          });
        }

        var customer = new Customer(doc);

        customer.save(function (err) {
          if (err) {
            return reject(err);
          }

          return resolve({
            message: 'Database Seeding: Customer\t' + customer.title + ' added'
          });
        });
      });
    }
  });
}
