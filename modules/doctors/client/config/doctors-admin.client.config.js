﻿(function () {
  'use strict';

  // Configuring the Doctors Admin module
  angular
    .module('doctors.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage Doctors',
      state: 'admin.doctors.list'
    });
  }
}());
