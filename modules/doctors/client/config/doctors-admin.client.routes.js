﻿(function () {
  'use strict';

  angular
    .module('doctors.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.doctors', {
        abstract: true,
        url: '/doctors',
        template: '<ui-view/>'
      })
      .state('admin.doctors.list', {
        url: '',
        templateUrl: '/modules/doctors/client/views/admin/list-doctors.client.view.html',
        controller: 'DoctorsAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.doctors.create', {
        url: '/create',
        templateUrl: '/modules/doctors/client/views/admin/form-doctor.client.view.html',
        controller: 'DoctorsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          doctorResolve: newDoctor
        }
      })
      .state('admin.doctors.edit', {
        url: '/:doctorId/edit',
        templateUrl: '/modules/doctors/client/views/admin/form-doctor.client.view.html',
        controller: 'DoctorsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin'],
          pageTitle: '{{ doctorResolve.title }}'
        },
        resolve: {
          doctorResolve: getDoctor
        }
      });
  }

  getDoctor.$inject = ['$stateParams', 'DoctorsService'];

  function getDoctor($stateParams, DoctorsService) {
    return DoctorsService.get({
      doctorId: $stateParams.doctorId
    }).$promise;
  }

  newDoctor.$inject = ['DoctorsService'];

  function newDoctor(DoctorsService) {
    return new DoctorsService();
  }
}());
