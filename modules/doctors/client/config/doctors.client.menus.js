(function () {
  'use strict';

  angular
    .module('doctors')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    menuService.addMenuItem('topbar', {
      title: 'Doctors',
      state: 'doctors',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'doctors', {
      title: 'List Doctors',
      state: 'doctors.list',
      roles: ['*']
    });
  }
}());
