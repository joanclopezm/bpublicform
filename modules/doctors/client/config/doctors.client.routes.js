(function () {
  'use strict';

  angular
    .module('doctors.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('doctors', {
        abstract: true,
        url: '/doctors',
        template: '<ui-view/>'
      })
      .state('doctors.list', {
        url: '',
        templateUrl: '/modules/doctors/client/views/list-doctors.client.view.html',
        controller: 'DoctorsListController',
        controllerAs: 'vm'
      })
      .state('doctors.view', {
        url: '/:doctorId',
        templateUrl: '/modules/doctors/client/views/view-doctor.client.view.html',
        controller: 'DoctorsController',
        controllerAs: 'vm',
        resolve: {
          doctorResolve: getDoctor
        },
        data: {
          pageTitle: '{{ doctorResolve.title }}'
        }
      });
  }

  getDoctor.$inject = ['$stateParams', 'DoctorsService'];

  function getDoctor($stateParams, DoctorsService) {
    return DoctorsService.get({
      doctorId: $stateParams.doctorId
    }).$promise;
  }
}());
