(function () {
  'use strict';

  angular
    .module('doctors.admin')
    .controller('DoctorsAdminController', DoctorsAdminController);

  DoctorsAdminController.$inject = ['$scope', '$state', '$window', 'doctorResolve', 'Authentication', 'Notification'];

  function DoctorsAdminController($scope, $state, $window, doctor, Authentication, Notification) {
    var vm = this;

    vm.doctor = doctor;
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Doctor
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.doctor.$remove(function () {
          $state.go('admin.doctors.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Doctor deleted successfully!' });
        });
      }
    }

    // Save Doctor
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.doctorForm');
        return false;
      }

      // Create a new doctor, or update the current instance
      vm.doctor.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('admin.doctors.list'); // should we send the User to the list or the updated Doctor's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Doctor saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Doctor save error!' });
      }
    }
  }
}());
