(function () {
  'use strict';

  angular
    .module('doctors')
    .controller('DoctorsController', DoctorsController);

  DoctorsController.$inject = ['$scope', 'doctorResolve', 'Authentication'];

  function DoctorsController($scope, doctor, Authentication) {
    var vm = this;

    vm.doctor = doctor;
    vm.authentication = Authentication;

  }
}());
