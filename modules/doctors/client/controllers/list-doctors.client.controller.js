(function () {
  'use strict';

  angular
    .module('doctors')
    .controller('DoctorsListController', DoctorsListController);

  DoctorsListController.$inject = ['DoctorsService'];

  function DoctorsListController(DoctorsService) {
    var vm = this;

    vm.doctors = DoctorsService.query();
  }
}());
