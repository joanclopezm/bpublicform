(function (app) {
  'use strict';

  app.registerModule('doctors', ['core']);// The core module is required for special route handling; see /core/client/config/core.client.routes
  app.registerModule('doctors.admin', ['core.admin']);
  app.registerModule('doctors.admin.routes', ['core.admin.routes']);
  app.registerModule('doctors.services');
  app.registerModule('doctors.routes', ['ui.router', 'core.routes', 'doctors.services']);
}(ApplicationConfiguration));
