'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Doctor = mongoose.model('Doctor'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create an doctor
 */
exports.create = function (req, res) {
  var doctor = new Doctor(req.body);
  doctor.user = req.user;

  doctor.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(doctor);
    }
  });
};

/**
 * Show the current doctor
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var doctor = req.doctor ? req.doctor.toJSON() : {};

  // Add a custom field to the Doctor, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Doctor model.
  doctor.isCurrentUserOwner = !!(req.user && doctor.user && doctor.user._id.toString() === req.user._id.toString());

  res.json(doctor);
};

/**
 * Update an doctor
 */
exports.update = function (req, res) {
  var doctor = req.doctor;
  
  
  doctor.title = req.body.title;
  doctor.content = req.body.content;
  doctor.link = req.body.link;
  doctor.shopifyId = req.body.shopifyId;
  doctor.name = req.body.name;

  doctor.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(doctor);
    }
  });
};

/**
 * Delete an doctor
 */
exports.delete = function (req, res) {
  var doctor = req.doctor;

  doctor.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(doctor);
    }
  });
};


exports.getSpecificDoctors = function (shopifyIds, callback){
  Doctor.find({$or:shopifyIds}).sort('-created').exec(function (err, doctors) {
      callback(err, doctors);
  });

}


/**
 * List of Doctors
 */
exports.list = function (req, res) {
  Doctor.find().sort('-created').populate('user', 'displayName').exec(function (err, doctors) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(doctors);
    }
  });
};

/**
 * Doctor middleware
 */
exports.doctorByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Doctor is invalid'
    });
  }

  Doctor.findById(id).populate('user', 'displayName').exec(function (err, doctor) {
    if (err) {
      return next(err);
    } else if (!doctor) {
      return res.status(404).send({
        message: 'No doctor with that identifier has been found'
      });
    }
    req.doctor = doctor;
    next();
  });
};
